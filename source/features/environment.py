import os
import signal
from subprocess import Popen, check_call
from time import sleep
from django.conf import settings
from behave import fixture, use_fixture
from selenium.webdriver import Remote, DesiredCapabilities, Chrome


@fixture
def browser_chrome(context):
    remote_url = os.getenv('REMOTE_CHROME_URL', None)
    if remote_url:
        desired_capabilities = DesiredCapabilities.CHROME.copy()
        context.browser = Remote(command_executor=remote_url, desired_capabilities=desired_capabilities)
    else:
        context.browser = Chrome()
    context.browser.maximize_window()
    yield context.browser
    if remote_url:
        context.browser.quit()


def before_all(context):
    use_fixture(browser_chrome, context)
    context.fixtures = ['auth.json', 'dump.json']
